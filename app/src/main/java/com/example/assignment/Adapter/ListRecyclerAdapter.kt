package com.example.assignment.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.assignment.Model.ListModel
import com.example.assignment.R
import de.hdodenhof.circleimageview.CircleImageView


class ListRecyclerAdapter() : RecyclerView.Adapter<ListRecyclerAdapter.MyViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<ListModel>()


    constructor(context: Context, dataList: ArrayList<ListModel>) : this() {
        this.context = context
        this.dataList = dataList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_item_view, parent, false)

        val holder = MyViewHolder(v)
        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       // holder.itemNameTV.setText(dataList.get(position).itemName)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var circleProfileView: CircleImageView
        lateinit var userNameTV: TextView
        lateinit var titleTV: TextView
        lateinit var postImgView: ImageView
        lateinit var descriptionTV: TextView
        lateinit var likeTV: TextView
        lateinit var voteTV: TextView
        lateinit var commentTV: TextView

        init {
            circleProfileView = itemView.findViewById(R.id.circleProfileView)
            userNameTV = itemView.findViewById(R.id.userNameTV)
            titleTV = itemView.findViewById(R.id.titleTV)
            postImgView = itemView.findViewById(R.id.postImgView)
            descriptionTV = itemView.findViewById(R.id.descriptionTV)
            likeTV = itemView.findViewById(R.id.likeTV)
            voteTV = itemView.findViewById(R.id.voteTV)
            commentTV = itemView.findViewById(R.id.commentTV)
        }
    }
}
