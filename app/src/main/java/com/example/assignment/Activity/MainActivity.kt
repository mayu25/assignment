package com.example.assignment

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.assignment.Adapter.ListRecyclerAdapter
import com.example.assignment.Model.ListModel
import org.json.JSONArray
import java.io.InputStream
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    val activity = this@MainActivity
    private lateinit var recyclerView: RecyclerView

    private var orderList = ArrayList<ListModel>()
    var adapter: ListRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        val linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.setLayoutManager(linearLayoutManager)
        adapter = ListRecyclerAdapter(activity, orderList)
        recyclerView.adapter = adapter

        setListData()
    }

    fun init() {
        recyclerView = findViewById(R.id.recyclerView)
    }

    fun setListData(){
        var jsonArray = JSONArray(readJSONFromAsset())
        for (i in 0..(jsonArray.length() - 1)) {

        }
    }


    //Read Data from json file.......

    fun readJSONFromAsset(): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("posts.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }
}
